CMAKE_MINIMUM_REQUIRED(VERSION 3.15)

# Set a default build type if none was specified
# ------------------------------------------------------------------------------
if(NOT CMAKE_BUILD_TYPE)
  message(STATUS "Setting build type to 'Release' as none was specified.")
  set(CMAKE_BUILD_TYPE Release CACHE STRING "Choose the type of build." FORCE)
endif()

message(STATUS "Install directory: ${CMAKE_INSTALL_PREFIX}")
# About this project
# ------------------------------------------------------------------------------
project(bigrepair)
SET(VERSION_MAJOR "0")
SET(VERSION_MINOR "1")
SET(VERSION_PATCH "0")
SET(VERSION "${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}")

# Set environment
# ------------------------------------------------------------------------------


find_package(Git)
if(GIT_FOUND)
    message("git found: ${GIT_EXECUTABLE}")
else()
    message(WARNING "git not found. Cloning of submodules will not work.")
endif()


# Configure the compiler with the appropriate flags
# ------------------------------------------------------------------------------
add_compile_options("-Wall")
add_compile_options("-Wextra")
# add_compile_options("-std=c99")
# add_compile_options("-std=c++11")

# Add the basic compiler options
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ")
# Add the basic compiler options for debug version
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -ggdb3")
set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -g")
# Add the basic compiler options for release version
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -march=native -funroll-loops -O3 -DNDEBUG")
set(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} -march=native -funroll-loops -O3 -DNDEBUG")

if(NOT DISABLE_PFP)
  add_subdirectory(ctph)
endif()
add_subdirectory(repair)
# add_subdirectory(large_repair)
add_subdirectory(largeb_repair)

include_directories(${PROJECT_SOURCE_DIR})

add_compile_options("-D Unique=0x78000000")
add_compile_options("-std=c99")
# add_compile_options("-std=c++11")

add_executable(procdic procdic.c)
add_executable(postproc postproc.c)
add_executable(iprocdic iprocdic.c)
add_executable(ipostproc ipostproc.c)

# Configure pipeline for build folder
set(USE_INSTALL_PATH False)
configure_file(${PROJECT_SOURCE_DIR}/bigrepair.in ${PROJECT_BINARY_DIR}/bigrepair @ONLY)

# Configure pipeline for install folder
set(USE_INSTALL_PATH True)
configure_file(${PROJECT_SOURCE_DIR}/bigrepair.in ${PROJECT_BINARY_DIR}/bigrepair.install @ONLY)

install(TARGETS procdic postproc iprocdic ipostproc TYPE RUNTIME)
install(PROGRAMS ${PROJECT_BINARY_DIR}/bigrepair.install RENAME bigrepair TYPE BIN)